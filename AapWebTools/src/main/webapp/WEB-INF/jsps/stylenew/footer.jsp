<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<div class="footerfullbg">
	<!--footerfullbg-->
	<div class="footercontent">
		<!--footercontent-->
		<div class="footerrow">
			<!--footerrow-->
			<h2>Donation Links</h2>
			<ul>
				<li><a href="https://donate.aamaadmiparty.org/">Donate Online</a></li>
				<li><a href="http://www.aamaadmiparty.org/donate-by-chequedemand-draft">Donate - By Cheque/Demand Draft</a></li>
				<li><a href="http://www.aamaadmiparty.org/donation-policies">Donation Policies</a></li>
				<li><a href="http://www.aamaadmiparty.org/donation-list">List of Donors</a></li>
				<li><a href="http://www.aamaadmiparty.org/donation-faq">Donation FAQs</a></li>
				<li><a href="http://aamaadmiparty.org/income%20expenditure%20details">Income & Expenditure Statements</a></li>
				<li><a href="http://ifuelswaraj.org/">I Fuel Swaraj</a></li>
			</ul>
		</div>
		<!--footerrow-->

		<div class="footerrow">
			<!--footerrow-->
			<h2>Media</h2>
			<ul>
				<li><a href="http://aapkikranti.com/">Aap Ki Kranti</a></li>
				<li><a href="http://aamaadmiparty.wordpress.com/">Publicity Material</a></li>
				<li><a href="http://www.aamaadmiparty.org/official-spokespersons">Official Spokespersons</a></li>
				<li><a href="http://www.aamaadmiparty.org/video-gallery">Videos</a></li>
			</ul>
		</div>
		<!--footerrow-->

		<div class="footerrow">
			<!--footerrow-->
			<h2>Contact Us</h2>
			<ul>
				<li><a href="http://www.aamaadmiparty.org/contact-us">Party Offices</a></li>
				<li><a href="http://nri.aamaadmiparty.org/">NRI Site</a></li>
				<li><a href="http://ifuelswaraj.org/leaders.php">"I Fuel Swaraj" Contacts</a></li>
				<li><a href="http://www.aamaadmiparty.org/privacy-policy">Privacy Policy</a></li>
			</ul>
		</div>
		<!--footerrow-->
	</div>
	<!--footercontent-->
</div>

<!--footerfullbg-->
<div class="footerfull">
	<!--footerfull-->
	<div class="footercontent">
		<!--footercontent-->
		<div class="conectleft">
			<!--conectleft-->
			<ul>
				<li class="h3">Connect with us on</li>
				<li><a href="https://www.facebook.com/AamAadmiParty"><img src="<c:out value='${staticDirectory}'/>/images/facebooklogo.png" border="0" align="absmiddle" /></a></li>
				<li><a href="https://twitter.com/AamAadmiParty"><img src="<c:out value='${staticDirectory}'/>/images/twitterlogo.png" border="0" align="absmiddle" /></a></li>
				<li><a href="https://www.youtube.com/user/indiACor2010"><img src="<c:out value='${staticDirectory}'/>/images/youtubeicon.png" border="0" align="absmiddle" /></a></li>
			</ul>
		</div>
		<!--conectleft-->
		<div class="copyright">
			<!--copyright-->
			© Aam Aadmi Party. All Rights Reserved, Developed by <a href="http://my.aamaadmiparty.org/ripple/ravi.html">Ravi Sharma</a>
		</div>
		<!--copyright-->
	</div>
	<!--footercontent-->
</div>
<!--footerfull-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-38065713-7', 'aamaadmiparty.org');
  ga('send', 'pageview');

</script>